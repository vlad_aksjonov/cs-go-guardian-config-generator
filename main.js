/**
 * Created by BlackSwan on 12.07.2015.
 */
var type = "type";
var values = "values";
var hint = "hint";
var aliases = "aliases";

function dce(element){
    return document.createElement(element);
}

function dctn(text){
    return document.createTextNode(text);
}

var ConfigFields = [];
var KnownFields = {
    'bot_custom_strat': {type: 'str_list', values: ['rush_a', 'rush_b'], hint: 'Overrides default bot strategy.'},
    'mp_randomspawn_in_bombsite': {type: 'custom', hint:'Index of bombsite to spawn, depends on map settings.'},
    'mp_randomspawn': {type: 'int_list', values: [2, 3], aliases: ['T', 'CT'], hint: 'Team, to spawn on bombsite'},
    'mp_maxrounds': {type: 'custom', hint: "Maximum amount of round's to complete objective"},
    'mp_roundtime': {type: 'custom'},
    'bot_difficulty': {type: 'int_list', values: [0, 1, 2, 3], aliases: ['Easy', 'Normal', 'Hard', 'Expert']},
    'mp_use_respawn_waves': {
        type: 'int_list',
        values: [1, 2],
        aliases: ['After death', 'After whole team death'],
        hint: 'Way to respawn enemies.'
    },
    'mp_startmoney': {type: 'custom'},
    'mp_afterroundmoney': {type: 'custom'},
    'mp_buytime': {type: 'custom', hint:'Buytime in seconds'},
    'sv_buy_status_override': {
        type: 'int_list',
        values: [0, 1, 2, 3],
        aliases: ['Everyone', 'CT-only', 'T-only', 'Nobody'],
        hint: 'Overrides map default buy settings'
    },
    'mp_ct_default_primary': {
        type: 'str_list', values: [
            "", "weapon_m4a1_silencer", "weapon_m4a1", "weapon_ak47", "weapon_aug", "weapon_awp", "weapon_bizon", "weapon_famas",
            "weapon_gs3sg1", "weapon_galilar", "weapon_m249", "weapon_mac10", "weapon_mag7", "weapon_mp7", "weapon_mp9",
            "weapon_negev", "weapon_nova", "weapon_scar20", "weapon_sg556", "weapon_ssg08", "weapon_ump45", "weapon_xm1014"]
    },
    'mp_ct_default_secondary': {
        type: 'str_list', values: [
            "", "weapon_deagle", "weapon_elite", "weapon_fiveseven", "weapon_glock", "weapon_hkp2000", "weapon_tec9"]
    },
    'mp_t_default_primary': {
        type: 'str_list', values: [
            "", "weapon_m4a1_silencer", "weapon_m4a1", "weapon_ak47", "weapon_aug", "weapon_awp", "weapon_bizon", "weapon_famas",
            "weapon_gs3sg1", "weapon_galilar", "weapon_m249", "weapon_mac10", "weapon_mag7", "weapon_mp7", "weapon_mp9",
            "weapon_negev", "weapon_nova", "weapon_scar20", "weapon_sg556", "weapon_ssg08", "weapon_ump45", "weapon_xm1014"]
    },
    'mp_t_default_secondary': {
        type: 'str_list', values: [
            "", "weapon_deagle", "weapon_elite", "weapon_fiveseven", "weapon_glock", "weapon_hkp2000", "weapon_tec9"]
    },
    'mp_free_armor': {type: 'bool'},
    'sv_auto_adjust_bot_difficulty': {type: 'bool'},
    'sv_bots_get_easier_each_win': {type: 'custom'},
    'sv_bots_get_harder_after_each_wave': {type: 'custom'},
    'sv_bots_force_rebuy_every_round': {type: 'bool'},
    'mp_buy_anywhere': {type: 'bool'},
    'mp_buy_during_immunity': {type: 'bool'},
    'bot_allow_rifles': {type: 'bool'},
    'bot_allow_snipers': {type: 'bool'},
    'bot_allow_machine_guns': {type: 'bool'},
    'cash_player_killed_enemy_default': {type: 'custom'},
    'cash_player_get_killed': {type: 'custom'},
    'cash_player_bomb_planted': {type: 'custom'},
    'cash_team_survive_guardian_wave': {type: 'custom'},
    'cash_team_elimination_bomb_map': {type: 'custom'},
    'cash_team_loser_bonus': {type: 'custom'},
    'cash_team_loser_bonus_consecutive_rounds': {type: 'custom'},
    'mp_maxmoney': {type: 'custom'},
    'mp_default_team_winner_no_objective': {type: 'int_list', values: [0, 2, 3], aliases: ['Draw', 'T', 'CT'], hint: 'Which team will win after time exceed'},
    'mp_guardian_special_kills_needed': {type: 'custom', hint: 'Amount of kills from special weapon needed to complete objective'},
    'mp_guardian_special_weapon_needed': {
        type: 'str_list', values: [
            '', 'glock', 'hkp2000', 'p250', 'elite', 'deagle', 'tec9', 'fiveseven', 'mp9', 'mac10', 'mp7', 'p90',
            'bizon', 'ump45', 'nova', 'xm1014', 'mag7', 'sawedoff', 'negev', 'm249', 'sg008', 'awp', 'ak47',
            'galilar', 'sg556', 'g3sg1', 'famas', 'm4a1', 'm4a1-s', 'aug', 'scar20', 'hegrenade', 'flashbang',
            'smokegrenade', 'decoy', 'molotov', 'incgrenade'
        ]
    },
    'mp_guardian_player_dist_min': {type: 'custom', hint: 'The distance at which game start to warn a player when they are too far from the guarded bombsite.'},
    'mp_guardian_player_dist_max': {type: 'custom', hint: 'The maximum distance a player is allowed to get from the bombsite before they are killed.'},
    'mp_respawn_on_death_t': {type: 'bool'},
    'mp_respawn_on_death_ct': {type: 'bool'},
    'bot_allow_rogues': {type:'bool', hint:'Allows random bots to not follow map objectives.'},
    'mp_hostages_spawn_same_every_round': {type:'bool'},
    'mp_hostages_spawn_force_positions': {type:'custom', hint: 'Index of hostage spawn position. Depends on map settings'},
    'mp_guardian_bot_money_per_wave': {type:'custom'}
    //'sv_bots_get_harder_after_each_wave': {type: 'int_list', values: ['2', '3'], aliases: ['T', 'CT'], hint: ''},
};

var MapList = [
    'cs_assault', 'cs_italy', 'cs_militia', 'cs_office', 'de_aztec', 'de_cache', 'de_cbble', 'de_dust', 'de_dust2',
    'de_inferno', 'de_lake', 'de_overpass', 'de_seazon', 'de_train', 'de_vertigo', 'gd_bank', 'gd_cbble', 'gd_crashsite',
    'gd_lake'
];

function GetFile(url) {
    var xml;
    if (window.XMLHttpRequest) {
        xml = new window.XMLHttpRequest();
        xml.open("GET", url, false);
        xml.send("");
        return xml.responseText;
    }
    else {
        if (window.ActiveXObject) {
            xml = new ActiveXObject("Microsoft.XMLDOM");
            xml.async = false;
            xml.load(url);
            return xml;
        }
        else {
            return null;
        }
    }
};

function LoadVanillaConfig(target) {
    var map = target.dataset['map'];
    var mode = (document.getElementById('switch_mode').parentNode.classList.contains('is-checked'))?'advanced':'simple';
    location.hash = map+"|"+mode;
}

function SwitchMode(){
    var tag = location.hash.slice(1);
    var params = tag.split('|');
    var map = "";
    var mode = "";
    if(params.length>1)
    {
        map = params[0];
        mode = params[1];
    }
    else
    {
        map = params[0];
        mode = "advanced";
    }
    if(MapList.indexOf(map)==-1) map = MapList[0];
    mode = (document.getElementById('switch_mode').parentNode.classList.contains('is-checked'))?'advanced':'simple';
    location.hash = map+"|"+mode;
}

function ParseConfig(config) {
    var lines = config.split("\n");
    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];
        var commented_out = false;
        if (line.slice(0, 2) == '//') {
            commented_out = true;
            line = line.slice(2);
        }

        if (/\S/.test(line)) {
            var elements = line.split(/[\s\t]+/);
            var parameter = elements[0];

            var value = elements[1];

            var comment = false;
            if (elements.length > 3) {
                comment = elements.slice(3).join(' ');
            }

            ConfigFields.push({
                "parameter": parameter,
                "value": value,
                "commented_out": commented_out,
                "comment": comment
            });
        }
    }

}

function CreateConfigTable(page, mode) {
    switch(mode)
    {
        case 'advanced':{
            AdvancedMode(page);
        }break;
        case 'simple':{

        }break;
    }
    componentHandler.upgradeAllRegistered();
    UpdateBlob();
}

function UpdateBlob(){
    var text = "";
    for(var i=0;i<ConfigFields.length;i++){
        var line = "";
        var row = ConfigFields[i];
        if(row.commented_out) line = "//";
        line+=row.parameter;
        line+="\t";
        line+=row.value;
        text+=line+"\r\n";
    }
    var blob = new Blob([text], {type: 'text/plain'});
    var a = document.getElementById('downloadLink');
    a.href = window.URL.createObjectURL(blob);
    a.setAttribute('download','test.cfg');
}

function AdvancedMode(page) {

    var a = dce('a');
    a.id='downloadLink';
    var button = CreateButton('downloadButton', 'Download');
    button.onclick=function(){
        UpdateBlob();
    }
    a.appendChild(button);
    page.appendChild(a);
    button = CreateButton('resetButton', 'Reset');
    button.onclick=function(){
        HashChanged();
    }
    page.appendChild(button);
    button = CreateButton('addButton', 'Add parameter');
    button.onclick=function(){
        alert('Not developed function');
    }
    page.appendChild(button);
    var table = dce('table');
    table.className = 'mdl-data-table mdl-js-data-table mdl-shadow--2dp';
    page.appendChild(table);

    var header = dce('thead');
    table.appendChild(header);
    table.style.whiteSpace = 'normal';
    var tr = dce('tr');
    header.appendChild(tr);
    var th = dce('th');
    th.className = 'mdl-data-table__cell--non-numeric';
    th.appendChild(dctn('Commented out'));
    tr.appendChild(th);
    th = dce('th');
    th.className = 'mdl-data-table__cell--non-numeric';
    th.appendChild(dctn('Parameter'));
    tr.appendChild(th);
    th = dce('th');
    th.className = 'mdl-data-table__cell--non-numeric';
    th.appendChild(dctn('Value'));
    tr.appendChild(th);
    th = dce('th');
    th.className = 'mdl-data-table__cell--non-numeric';
    th.appendChild(dctn('Comment'));
    tr.appendChild(th);

    var body = dce('tbody');
    table.appendChild(body);

    for (var i = 0; i < ConfigFields.length; i++) {
        var row = ConfigFields[i];

        tr = dce('tr');
        body.appendChild(tr);

        td = dce('td');
        tr.appendChild(td);
        td.className = 'mdl-data-table__cell--non-numeric';
        var checkbox = CreateCheckbox("switch_"+i, row.commented_out);
        checkbox.setAttribute('onclick',"CommentUpdated("+i+")");
        td.appendChild(checkbox);


        td = dce('td');
        tr.appendChild(td);
        td.className = 'mdl-data-table__cell--non-numeric';
        td.appendChild(dctn(row.parameter));

        td = dce('td');
        tr.appendChild(td);
        td.className = 'mdl-data-table__cell--non-numeric';

        var parinfo = KnownFields[row.parameter];
        if(parinfo){
            switch(parinfo.type)
            {
                case 'bool':{
                    checkbox = CreateCheckbox('value_'+i, row.value==1);
                    checkbox.setAttribute('onclick',"FieldUpdated("+i+")");
                    td.appendChild(checkbox);
                }break;
                case 'custom':{
                    var field = CreateTextfield('value_'+i, row.value);
                    field.setAttribute('oninput',"FieldUpdated("+i+")");
                    td.appendChild(field);
                }break;
                case 'int_list':{
                    var select = dce('select');
                    select.id = 'value_' + i;
                    select.setAttribute('onchange',"FieldUpdated("+i+")");
                    for(var j=0;j<parinfo.values.length;j++)
                    {
                        var option = dce('option');
                        option.appendChild(dctn(parinfo.aliases[j]));
                        option.value = parinfo.values[j];
                        if(parinfo.values[j]==row.value) option.setAttribute('selected','selected');
                        select.appendChild(option);
                    }
                    td.appendChild(select);
                }break;
                case 'str_list':{
                    var select = dce('select');
                    select.id = 'value_' + i;
                    select.setAttribute('onchange',"FieldUpdated("+i+")");
                    for(var j=0; j<parinfo.values.length; j++)
                    {
                        var option = dce('option');
                        option.appendChild(dctn(parinfo.values[j]));
                        option.value = parinfo.values[j];
                        if(parinfo.values[j]==row.value) option.setAttribute('selected','selected');
                        select.appendChild(option);
                    }
                    td.appendChild(select);
                }break;
            }
        }
        else
        {
            td.appendChild(dctn(row.value));
        }
        td = dce('td');
        tr.appendChild(td);
        td.className = 'mdl-data-table__cell--non-numeric';
        td.appendChild(dctn((parinfo.hint)?parinfo.hint:""));
    }
}

function FieldUpdated(id){
    var param = ConfigFields[id];
    var element = document.getElementById('value_'+id);
    switch(KnownFields[param.parameter].type)
    {
        case 'bool':{
            element = element.parentNode;
            ConfigFields[id].value = (element.classList.contains('is-checked'))?1:0;
        }break;
        case 'custom':{
            ConfigFields[id].value = element.value;
        }break;
        case 'int_list':case 'str_list':{
            ConfigFields[id].value = element.options[element.selectedIndex].value;
        }break;
    }
    UpdateBlob();
}

function CommentUpdated(id){
    var param = ConfigFields[id];
    var element = document.getElementById('switch_'+id);
    ConfigFields[id].commented_out = (element.classList.contains('is-checked'));
    UpdateBlob();
}

function CreateCheckbox(id, checked) {
    var label = dce('label');
    label.className = "mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect";
    label.setAttribute("for",id);
    var input = dce('input');
    label.appendChild(input);
    input.type = 'checkbox';
    input.id = id;
    input.className="mdl-checkbox__input";
    if (checked) {
        input.setAttribute('checked', 'checked');
    }
    return label;
}

function CreateTextfield(id, value) {
    var div = dce('div');
    div.className = "mdl-textfield mdl-js-textfield textfield-demo";
    var input = dce('input');
    div.appendChild(input);
    input.type = 'text';
    input.id = id;
    input.className="mdl-textfield__input";
    input.value = value;
    return div;
}

function CreateButton(id,text) {
    var button = dce('button');
    button.id = id;
    button.className = "mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect";
    button.appendChild(dctn(text));
    return button;
}

function HashChanged() {
    var tag = location.hash.slice(1);
    var params = tag.split('|');
    var map = "";
    var mode = "";
    if(params.length>1)
    {
        map = params[0];
        mode = params[1];
    }
    else
    {
        map = params[0];
        mode = "advanced";
    }
    if(MapList.indexOf(map)==-1) map = MapList[0];

    var links = document.getElementsByClassName('mdl-navigation__link');
    for(var i=0;i<links.length;i++)
    {
       if(links[i].dataset["map"] == map){
           links[i].classList.add('is-active');
       }
        else
       {
           links[i].classList.remove('is-active');
       }
    }

    if(mode=='advanced')
    {
        document.getElementById('switch_mode').parentNode.classList.add('is-checked');
    }
    else
    {
        document.getElementById('switch_mode').parentNode.classList.remove('is-checked');
    }
    var config = GetFile("vanilla/guardian_" + map + ".cfg");
    var page = document.getElementById("page-content");
    page.innerHTML = "";
    ConfigFields = [];
    ParseConfig(config);
    CreateConfigTable(page, mode);
}

window.onhashchange = HashChanged;
